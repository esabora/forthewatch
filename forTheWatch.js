
watchInterval = 1; //watch for variables every X seconds // don't hesitate to modify this number as needed.


if (typeof forTheWatch !== 'undefined') {
    clearInterval(forTheWatch);
}
forTheWatch = setInterval(watChanged, watchInterval*1000);

if(typeof varsToWatch === 'undefined'){
    varsToWatch = [];
}

//what to do if a var changed. (launched every X sec)
function watChanged(){
    for(key in varsToWatch){
        var newVal = eval(key);
        if(varsToWatch[key]["valeur"] !== newVal){
            //console.log(key+" : check : "+varsToWatch[key]["valeur"]+ "/"+newVal);
            tabParams = [key, varsToWatch[key]["valeur"], newVal];// we always put the name of var, its old value and new value, before adding the others params if existing
            if(typeof varsToWatch[key]["params"] !== 'undefined'){
                tabParams = tabParams.concat(varsToWatch[key]["params"]);
            }
            executeFunctionByName(varsToWatch[key]["fonction"], window, tabParams);
            varsToWatch[key]["valeur"] = newVal;
        }
    }
}

//adding the var to the watching tab
function watchIt(varName, varFunction, oneOrSeveral = "one", varParams = null){
    //if the var to watch is just indeed a var
    if(oneOrSeveral === "one"){
        varsToWatch[varName] = [];
        varsToWatch[varName]["valeur"] = eval(varName);
        varsToWatch[varName]["fonction"] = varFunction;
        if(varParams !== null){
            varsToWatch[varName]["params"] = varParams;
        }
    //if the var to watch is an array of var names to watch that launch the same function if changed
    }else if(oneOrSeveral === "several"){
        for(i=0;i<varName.length;i++){
            theVarToWatch = varName[i];
            varsToWatch[theVarToWatch] = [];
            varsToWatch[theVarToWatch]["valeur"] = eval(theVarToWatch);
            varsToWatch[theVarToWatch]["fonction"] = varFunction;
            if(varParams !== null){
                varsToWatch[theVarToWatch]["params"] = varParams;
            }
        }
    }
}

//this function to launch a function with its name as a string, considering the possibility of namespaces.
function executeFunctionByName(functionName, context /*, args */) {
    var args = Array.prototype.slice.call(arguments, 2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
}
