# ForTheWatch

ForTheWatch is a tiny "dinosaur-encoded" JS library in order to watch Global variables, and launch specific functions when these variables change.
You simply activate the watch on a variable and specify the function to launch.
An equivalent to eventHandler, but for simple global variables.

Sorry if it already exists, of if better/official ways to do it exists.

watchIt("theVariable", "varChanged", "anotherOptionalParameter");

//Please see the example for more information.